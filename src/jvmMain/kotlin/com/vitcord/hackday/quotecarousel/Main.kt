package com.vitcord.hackday.quotecarousel

import com.fasterxml.jackson.databind.SerializationFeature
import com.vitcord.hackday.quotecarousel.actions.GetAllQuotesAction
import com.vitcord.hackday.quotecarousel.actions.GetQuotesByAuthorAction
import com.vitcord.hackday.quotecarousel.data.InMemoryQuotesRepository
import com.vitcord.hackday.quotecarousel.responders.QuotesCarouselResponder
import com.vitcord.hackday.quotecarousel.responders.QuotesListHtmlResponder
import com.vitcord.hackday.quotecarousel.responders.QuotesListJsonResponder
import com.vitcord.hackday.quotecarousel.usecases.GetAllQuotes
import com.vitcord.hackday.quotecarousel.usecases.GetQuotesByAuthor
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.http.content.files
import io.ktor.http.content.static
import io.ktor.jackson.jackson
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.io.File

fun main() {
  val repository = InMemoryQuotesRepository()

  val getAllQuotes = GetAllQuotes(repository)
  val getQuotesByAuthor = GetQuotesByAuthor(repository)

  val quotesHtmlResponder = QuotesListHtmlResponder()
  val quotesJsonResponder = QuotesListJsonResponder()
  val quotesCarouselResponder = QuotesCarouselResponder()

  embeddedServer(Netty, port = 8080, host = "127.0.0.1") {

    val currentDir = File(".").absoluteFile
    environment.log.info("Current directory: $currentDir")

    val webDir = listOf(
      "web",
      "../src/jsMain/web",
      "src/jsMain/web"
    ).map {
      File(currentDir, it)
    }.firstOrNull { it.isDirectory }?.absoluteFile ?: error("Can't find 'web' folder for this sample")

    environment.log.info("Web directory: $webDir")

    install(ContentNegotiation) {
      jackson {
        enable(SerializationFeature.INDENT_OUTPUT)
      }
    }

    routing {
      get { GetAllQuotesAction(getAllQuotes, quotesCarouselResponder).handleRequest(call) }

      route("/quotes") {
        get { GetAllQuotesAction(getAllQuotes, quotesHtmlResponder).handleRequest(call) }
        get("/author/{id}") { GetQuotesByAuthorAction(getQuotesByAuthor, quotesHtmlResponder).handleRequest(call) }
      }

      route("/api/quotes") {
        get { GetAllQuotesAction(getAllQuotes, quotesJsonResponder).handleRequest(call) }
      }
      static("/static") {
        files(webDir)
      }
    }
  }.start(wait = true)
}