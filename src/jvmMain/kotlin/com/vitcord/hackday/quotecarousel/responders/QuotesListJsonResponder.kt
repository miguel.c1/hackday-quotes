package com.vitcord.hackday.quotecarousel.responders

import com.vitcord.hackday.quotecarousel.base.Responder
import com.vitcord.hackday.quotecarousel.model.Quote
import io.ktor.application.ApplicationCall
import io.ktor.response.respond

class QuotesListJsonResponder : Responder<List<Quote>> {
  override suspend fun respond(element: List<Quote>, call: ApplicationCall) =
    call.respond(element)
}