package com.vitcord.hackday.quotecarousel.responders

import com.vitcord.hackday.quotecarousel.base.Responder
import com.vitcord.hackday.quotecarousel.model.Quote
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import kotlinx.html.body
import kotlinx.html.br
import kotlinx.html.div
import kotlinx.html.head
import kotlinx.html.id
import kotlinx.html.p
import kotlinx.html.pre
import kotlinx.html.title

class QuotesListHtmlResponder : Responder<List<Quote>> {
  override suspend fun respond(element: List<Quote>, call: ApplicationCall) {
    call.respondHtml(HttpStatusCode.OK) {
      head {
        title("Vitcord Quotes")
      }

      body {
        if (element.isEmpty()) {
          p { +"There are no quotes right now" }
        } else {
          element.map { quote ->
            div {
              id = quote.id
              pre { +quote.text }
              p { +"${quote.author.name} - ${quote.onDate.getFullYear()}" }
            }
            br
          }
        }
      }
    }
  }
}