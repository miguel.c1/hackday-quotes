package com.vitcord.hackday.quotecarousel.responders

import com.vitcord.hackday.quotecarousel.base.Responder
import com.vitcord.hackday.quotecarousel.model.Quote
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import kotlinx.html.body
import kotlinx.html.br
import kotlinx.html.classes
import kotlinx.html.div
import kotlinx.html.head
import kotlinx.html.id
import kotlinx.html.link
import kotlinx.html.map
import kotlinx.html.p
import kotlinx.html.script
import kotlinx.html.span
import kotlinx.html.title

class QuotesCarouselResponder : Responder<List<Quote>> {
  override suspend fun respond(element: List<Quote>, call: ApplicationCall) {
    call.respondHtml(HttpStatusCode.OK) {
      head {
        title("Vitcord Quotes")
        link(rel = "stylesheet", href="/static/styles.css", type = "text/css")
        link(rel = "stylesheet", href="https://fonts.googleapis.com/css?family=Niconne&display=swap")
      }

      body {
        div {
          id = "carousel"

          div {
            id = "carousel-inner"

            element.mapIndexed { index, quote ->
              div("item") {
                if (index == 0)
                  classes = classes + "active"

                div("container") {

                  p("quote-phrase") {
                    span("quote-marks") { +"“" }
                    val lines = quote.text.split("\n")
                    lines.mapIndexed { index, line ->
                      +line
                      if (index != lines.size - 1) br
                    }
                    span("quote-marks") { +"”" }
                  }

                  p("author") { +"${quote.author.name} - ${quote.onDate.getFullYear()}" }
                }
              }
            }
          }
        }

        script(src = "/static/require.min.js") {}
        script {
          +"require.config({baseUrl: '/static'});\n"
          +"require(['/static/quotes-carousel.js'], function(js) { js.com.vitcord.hackday.quotecarousel.onDocumentReady(); });\n"
        }
      }
    }
  }
}