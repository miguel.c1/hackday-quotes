package com.vitcord.hackday.quotecarousel.model

import java.util.Calendar

actual class Date(val timestamp: Long) {
  private val calendar = Calendar.getInstance()
    .apply { time = java.util.Date(timestamp) }

  actual fun getDay(): Int = calendar.get(Calendar.DAY_OF_MONTH)

  actual fun getMonth(): Int = calendar.get(Calendar.MONTH)

  actual fun getFullYear(): Int = calendar.get(Calendar.YEAR)
}