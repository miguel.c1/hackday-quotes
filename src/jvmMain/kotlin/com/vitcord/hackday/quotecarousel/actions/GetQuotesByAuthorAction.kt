package com.vitcord.hackday.quotecarousel.actions

import com.vitcord.hackday.quotecarousel.base.Action
import com.vitcord.hackday.quotecarousel.base.Responder
import com.vitcord.hackday.quotecarousel.model.Quote
import com.vitcord.hackday.quotecarousel.usecases.GetQuotesByAuthor
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode

class GetQuotesByAuthorAction(
  private val getQuotesByAuthor: GetQuotesByAuthor,
  private val responder: Responder<List<Quote>>
) : Action {
  override suspend fun handleRequest(call: ApplicationCall) {
    val authorId = call.parameters["id"]

    if (authorId != null) {
      val quotes = getQuotesByAuthor(authorId)
      responder.respond(quotes, call)
    } else {
      call.respondHtml(HttpStatusCode.BadRequest) { +"Missing id in path" }
    }
  }
}