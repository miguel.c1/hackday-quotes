package com.vitcord.hackday.quotecarousel.actions

import com.vitcord.hackday.quotecarousel.base.Action
import com.vitcord.hackday.quotecarousel.base.Responder
import com.vitcord.hackday.quotecarousel.model.Quote
import com.vitcord.hackday.quotecarousel.usecases.GetAllQuotes
import io.ktor.application.ApplicationCall

class GetAllQuotesAction(
  private val getAllQuotes: GetAllQuotes,
  private val responder: Responder<List<Quote>>
) : Action {
  override suspend fun handleRequest(call: ApplicationCall) {
    val quotes = getAllQuotes()
    responder.respond(quotes, call)
  }
}