package com.vitcord.hackday.quotecarousel.base

import io.ktor.application.ApplicationCall

interface Action {
  suspend fun handleRequest(call: ApplicationCall)
}