package com.vitcord.hackday.quotecarousel.base

import io.ktor.application.ApplicationCall

interface Responder<T> {
  suspend fun respond(element: T, call: ApplicationCall)
}