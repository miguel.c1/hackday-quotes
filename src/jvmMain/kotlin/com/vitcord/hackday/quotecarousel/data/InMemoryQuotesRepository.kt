package com.vitcord.hackday.quotecarousel.data

import com.vitcord.hackday.quotecarousel.model.Author
import com.vitcord.hackday.quotecarousel.model.Date
import com.vitcord.hackday.quotecarousel.model.Quote

class InMemoryQuotesRepository : QuotesRepository {

  private val carlos = Author("1", "Carlos")
  private val kike = Author("2", "Kike")
  private val samuel = Author("3", "Samuel")
  private val toño = Author("4", "Toño")
  private val pablo = Author("5", "Pablo")
  private val cotel = Author("6", "Miguel Coleto")
  private val canales = Author("7", "Miguel Canales")
  private val tomas = Author("8", "Tomás")
  private val adri = Author("9", "Adri")

  private val allQuotes = listOf(
    Quote("1", "Yo estoy con eso... y con esto estoy.", carlos, Date(1562234400000)),
    Quote("2", "Esto no es bueno ni es malo... es bueno.", kike, Date(1562061600000)),
    Quote("3", "- Estoy mejor que ayer\n+ ¿Pero peor que mañana?\n- ¡NO POR FAVOR!", carlos, Date(1560247200000)),
    Quote("4", "El perfil solo debería de llamarse en el perfil", samuel, Date(1559642400000)),
    Quote("5", "Yo ayer estaba mirando... lo que estaba mirando.", carlos, Date(1559642400000)),
    Quote("6", "Por nosotros mucho genial.", toño, Date(1559642400000)),
    Quote("7", "Está claro que algo esta pasando en algún lado.", pablo, Date(1559642400000)),
    Quote("8", "En cuanto tal, pum.", carlos, Date(1559642400000)),
    Quote("9", "Puedo poner que tengo noventa y diez años.", cotel, Date(1559642400000)),
    Quote("10", "Como me gustaría ser negro.", canales, Date(1559642400000)),
    Quote("11", "Cuando automaticemos esto será automático.", tomas, Date(1559642400000)),
    Quote("12", "En el peor de los casos, hay empresas que han multiplicado por 1 la inversión, y han ganado.", kike, Date(1559642400000)),
    Quote("13", "50 más 12 es más que 12", adri, Date(1559642400000)),
    Quote("14", "Yo vivo de mi sonrisa", canales, Date(1559642400000)),
    Quote("15", "En Amplitude no se crashean los crashes", kike, Date(1559642400000)),
    Quote("16", "Siembras lo que recoges", cotel, Date(1559642400000)),
    Quote("17", "Contexto se le da si tiene, si no tiene no se le da", kike, Date(1544436000000)),
    Quote("18", "Lo abrió 5 personas, pero en España hay más personas", canales, Date(1544436000000)),
    Quote("19", "¿Vosotros no habéis ido al Barcelona Games World? Bueno, si estáis aquí, no podéis haber ido.", carlos, Date(1544436000000)),
    Quote("20", "Si queréis os puedo dar la charla de memoria, pero no me acuerdo", samuel, Date(1544436000000)),
    Quote("21", "- A este chaval le gusta cantar\n+ ¿A quién?\n- Al que canta", carlos, Date(1544436000000)),
    Quote("22", "No es a golpes, es hablada, es con boca", samuel, Date(1544436000000))
  )

  override suspend fun getAllQuotes(): List<Quote> =
    allQuotes

  override suspend fun getQuotesByAuthor(authorId: String): List<Quote> =
    allQuotes.filter { it.author.id == authorId }
}