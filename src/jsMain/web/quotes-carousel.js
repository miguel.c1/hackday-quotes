(function (root, factory) {
  if (typeof define === 'function' && define.amd)
    define(['exports', 'kotlin'], factory);
  else if (typeof exports === 'object')
    factory(module.exports, require('kotlin'));
  else {
    if (typeof kotlin === 'undefined') {
      throw new Error("Error loading module 'quotes-carousel'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'quotes-carousel'.");
    }
    root['quotes-carousel'] = factory(typeof this['quotes-carousel'] === 'undefined' ? {} : this['quotes-carousel'], kotlin);
  }
}(this, function (_, Kotlin) {
  'use strict';
  var Kind_INTERFACE = Kotlin.Kind.INTERFACE;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var Unit = Kotlin.kotlin.Unit;
  var NotImplementedError_init = Kotlin.kotlin.NotImplementedError;
  function QuotesRepository() {
  }
  QuotesRepository.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'QuotesRepository',
    interfaces: []
  };
  function Author(id, name) {
    this.id = id;
    this.name = name;
  }
  Author.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Author',
    interfaces: []
  };
  Author.prototype.component1 = function () {
    return this.id;
  };
  Author.prototype.component2 = function () {
    return this.name;
  };
  Author.prototype.copy_puj7f4$ = function (id, name) {
    return new Author(id === void 0 ? this.id : id, name === void 0 ? this.name : name);
  };
  Author.prototype.toString = function () {
    return 'Author(id=' + Kotlin.toString(this.id) + (', name=' + Kotlin.toString(this.name)) + ')';
  };
  Author.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.id) | 0;
    result = result * 31 + Kotlin.hashCode(this.name) | 0;
    return result;
  };
  Author.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.id, other.id) && Kotlin.equals(this.name, other.name)))));
  };
  function show($receiver) {
    return $receiver.getDay().toString() + ' - ' + $receiver.getMonth() + ' - ' + $receiver.getFullYear();
  }
  function Quote(id, text, author, onDate) {
    this.id = id;
    this.text = text;
    this.author = author;
    this.onDate = onDate;
  }
  Quote.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Quote',
    interfaces: []
  };
  Quote.prototype.component1 = function () {
    return this.id;
  };
  Quote.prototype.component2 = function () {
    return this.text;
  };
  Quote.prototype.component3 = function () {
    return this.author;
  };
  Quote.prototype.component4 = function () {
    return this.onDate;
  };
  Quote.prototype.copy_cspwcz$ = function (id, text, author, onDate) {
    return new Quote(id === void 0 ? this.id : id, text === void 0 ? this.text : text, author === void 0 ? this.author : author, onDate === void 0 ? this.onDate : onDate);
  };
  Quote.prototype.toString = function () {
    return 'Quote(id=' + Kotlin.toString(this.id) + (', text=' + Kotlin.toString(this.text)) + (', author=' + Kotlin.toString(this.author)) + (', onDate=' + Kotlin.toString(this.onDate)) + ')';
  };
  Quote.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.id) | 0;
    result = result * 31 + Kotlin.hashCode(this.text) | 0;
    result = result * 31 + Kotlin.hashCode(this.author) | 0;
    result = result * 31 + Kotlin.hashCode(this.onDate) | 0;
    return result;
  };
  Quote.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.id, other.id) && Kotlin.equals(this.text, other.text) && Kotlin.equals(this.author, other.author) && Kotlin.equals(this.onDate, other.onDate)))));
  };
  function GetAllQuotes(quotesRepository) {
    this.quotesRepository_0 = quotesRepository;
  }
  GetAllQuotes.prototype.invoke = function (continuation) {
    return this.quotesRepository_0.getAllQuotes(continuation);
  };
  GetAllQuotes.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GetAllQuotes',
    interfaces: []
  };
  function GetQuotesByAuthor(quotesRepository) {
    this.quotesRepository_0 = quotesRepository;
  }
  GetQuotesByAuthor.prototype.invoke_61zpoe$ = function (authorId, continuation) {
    return this.quotesRepository_0.getQuotesByAuthor_61zpoe$(authorId, continuation);
  };
  GetQuotesByAuthor.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GetQuotesByAuthor',
    interfaces: []
  };
  var SPEED;
  var isEnabled;
  var currentItem;
  var items;
  function changeCurrentItem(item) {
    currentItem = (item + items.length | 0) % items.length;
  }
  function nextItem(item) {
    hideItem('to-left');
    changeCurrentItem(item + 1 | 0);
    showItem('from-right');
  }
  function previousItem(item) {
    hideItem('to-right');
    changeCurrentItem(item - 1 | 0);
    showItem('from-left');
  }
  function goToItem(item) {
    if (item < currentItem) {
      hideItem('to-right');
      currentItem = item;
      showItem('from-left');
    }
     else {
      hideItem('to-left');
      currentItem = item;
      showItem('from-right');
    }
  }
  function hideItem$lambda(closure$element, closure$direction) {
    return function (f) {
      closure$element.classList.remove('active', closure$direction);
      return Unit;
    };
  }
  function hideItem(direction) {
    var tmp$;
    isEnabled = false;
    var element = items[currentItem];
    (tmp$ = element != null ? element.classList : null) != null ? (tmp$.add(direction), Unit) : null;
    element != null ? (element.addEventListener('animationend', hideItem$lambda(element, direction)), Unit) : null;
  }
  function showItem$lambda(closure$element, closure$direction) {
    return function (f) {
      closure$element.classList.remove('next', closure$direction);
      closure$element.classList.add('active');
      isEnabled = true;
      return Unit;
    };
  }
  function showItem(direction) {
    var tmp$;
    var element = items[currentItem];
    (tmp$ = element != null ? element.classList : null) != null ? (tmp$.add('next', direction), Unit) : null;
    element != null ? (element.addEventListener('animationend', showItem$lambda(element, direction)), Unit) : null;
  }
  function onDocumentReady$lambda() {
    if (isEnabled) {
      nextItem(currentItem);
      console.log('Next item!');
    }
    return Unit;
  }
  function onDocumentReady() {
    window.setInterval(onDocumentReady$lambda, SPEED);
    console.log(items);
  }
  function Date_0() {
  }
  Date_0.prototype.getDay = function () {
    throw new NotImplementedError_init('An operation is not implemented: ' + 'not implemented');
  };
  Date_0.prototype.getMonth = function () {
    throw new NotImplementedError_init('An operation is not implemented: ' + 'not implemented');
  };
  Date_0.prototype.getFullYear = function () {
    throw new NotImplementedError_init('An operation is not implemented: ' + 'not implemented');
  };
  Date_0.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Date',
    interfaces: []
  };
  var package$com = _.com || (_.com = {});
  var package$vitcord = package$com.vitcord || (package$com.vitcord = {});
  var package$hackday = package$vitcord.hackday || (package$vitcord.hackday = {});
  var package$quotecarousel = package$hackday.quotecarousel || (package$hackday.quotecarousel = {});
  var package$data = package$quotecarousel.data || (package$quotecarousel.data = {});
  package$data.QuotesRepository = QuotesRepository;
  var package$model = package$quotecarousel.model || (package$quotecarousel.model = {});
  package$model.Author = Author;
  package$model.show_4golet$ = show;
  package$model.Quote = Quote;
  var package$usecases = package$quotecarousel.usecases || (package$quotecarousel.usecases = {});
  package$usecases.GetAllQuotes = GetAllQuotes;
  package$usecases.GetQuotesByAuthor = GetQuotesByAuthor;
  package$quotecarousel.changeCurrentItem_za3lpa$ = changeCurrentItem;
  package$quotecarousel.nextItem_za3lpa$ = nextItem;
  package$quotecarousel.previousItem_za3lpa$ = previousItem;
  package$quotecarousel.goToItem_za3lpa$ = goToItem;
  package$quotecarousel.hideItem_61zpoe$ = hideItem;
  package$quotecarousel.showItem_61zpoe$ = showItem;
  package$quotecarousel.onDocumentReady = onDocumentReady;
  package$model.Date = Date_0;
  SPEED = 5000;
  isEnabled = true;
  currentItem = 0;
  items = document.getElementsByClassName('item');
  Kotlin.defineModule('quotes-carousel', _);
  return _;
}));

//# sourceMappingURL=quotes-carousel.js.map
