package com.vitcord.hackday.quotecarousel

import org.w3c.dom.get
import kotlin.browser.*


private val SPEED = 5000
private var isEnabled = true
private var currentItem = 0
private val items = document.getElementsByClassName("item")

fun changeCurrentItem(item: Int) {
  currentItem = (item + items.length) % items.length
}

fun nextItem(item: Int) {
  hideItem("to-left")
  changeCurrentItem(item + 1)
  showItem("from-right")
}

fun previousItem(item: Int) {
  hideItem("to-right")
  changeCurrentItem(item - 1)
  showItem("from-left")
}

fun goToItem(item: Int) {
  if (item < currentItem) {
    hideItem("to-right")
    currentItem = item
    showItem("from-left")
  } else {
    hideItem("to-left")
    currentItem = item
    showItem("from-right")
  }
}

fun hideItem(direction: String) {
  isEnabled = false
  val element = items[currentItem]
  element?.classList?.add(direction)
  element?.addEventListener("animationend", { _ ->
    element.classList.remove("active", direction)
  })
}

fun showItem(direction: String) {
  val element = items[currentItem]
  element?.classList?.add("next", direction)
  element?.addEventListener("animationend", { _ ->
    element.classList.remove("next", direction)
    element.classList.add("active")
    isEnabled = true
  })
}

@Suppress("unused")
@JsName("onDocumentReady")
fun onDocumentReady() {
  window.setInterval({
    if (isEnabled) {
      nextItem(currentItem)
      console.log("Next item!")
    }
  }, SPEED)

  console.log(items)
}