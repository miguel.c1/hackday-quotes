package com.vitcord.hackday.quotecarousel.usecases

import com.vitcord.hackday.quotecarousel.data.QuotesRepository
import com.vitcord.hackday.quotecarousel.model.Quote

class GetQuotesByAuthor(private val quotesRepository: QuotesRepository) {
  suspend operator fun invoke(authorId: String): List<Quote> =
    quotesRepository.getQuotesByAuthor(authorId)
}