package com.vitcord.hackday.quotecarousel.usecases

import com.vitcord.hackday.quotecarousel.data.QuotesRepository
import com.vitcord.hackday.quotecarousel.model.Quote

class GetAllQuotes(private val quotesRepository: QuotesRepository) {
  suspend operator fun invoke(): List<Quote> =
    quotesRepository.getAllQuotes()
}