package com.vitcord.hackday.quotecarousel.data

import com.vitcord.hackday.quotecarousel.model.Quote

interface QuotesRepository {
  suspend fun getAllQuotes(): List<Quote>
  suspend fun getQuotesByAuthor(authorId: String): List<Quote>
}