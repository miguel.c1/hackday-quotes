package com.vitcord.hackday.quotecarousel.model

import com.vitcord.hackday.quotecarousel.model.Author
import com.vitcord.hackday.quotecarousel.model.Date

data class Quote(
  val id: String,
  val text: String,
  val author: Author,
  val onDate: Date
)