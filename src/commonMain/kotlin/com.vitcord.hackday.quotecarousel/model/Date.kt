package com.vitcord.hackday.quotecarousel.model

expect class Date {
  fun getDay(): Int
  fun getMonth(): Int
  fun getFullYear(): Int
}

fun Date.show(): String =
  "${getDay()} - ${getMonth()} - ${getFullYear()}"