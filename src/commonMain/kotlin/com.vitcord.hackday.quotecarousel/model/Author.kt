package com.vitcord.hackday.quotecarousel.model

data class Author(
  val id: String,
  val name: String
)